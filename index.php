
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Libreria</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/estilos.css">
</head>
<body>
<div class=contenedor>
  <div class=encabezado>
    <h2>Ingrese libro</h2>
  </div>  
    <form action="agregar_libro.php" method="POST">
      <div class=formulario>
        <div class="form-group">
          <label for="Titulo">Titulo</label>
          <input type="text" name="titulo" required="" placeholder="nombre del libro">
        </div>
        <div class="form-group">
          <label for="Autor">Autor</label>
          <input type="text" name="autor" required="" placeholder="autor">
        </div>
        <div class="form-group">
          <label for="Ano">Año</label>
          <input type="text" name="ano" required=""  placeholder="AAAA">
        </div>
        <div class="form-group">
          <label for="Idioma">Idioma</label>
          <input type="text" name="idioma" required=""  placeholder="idioma">
        </div>
        <div class="botones" >
          <button type="Registrar libro" class="btn btn-primary">Agregar libro</button>
        </div>
      </div>
    </form>
    
    </form>
    <form action="libros.php" method="POST">
      <div class=formulario>
          <button type="Listar Libros" class="btn btn-primary">Listar Libros</button>
        </div>
      </div>
    </form>
  </div>

</body>
</html>