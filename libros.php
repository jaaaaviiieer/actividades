<?php 
    include 'conexion.php';

    $query="SELECT * FROM libros";
    $consulta_libros = $conexion->query($query);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Libros</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/estilos.css">
    
</head>
<body>
    <div class="contenedor">
        <div class="table-responsive" style="padding: 1%">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">Titulo</th>
                        <th scope="col">Autor</th>
                        <th scope="col">Año</th>
                        <th scope="col">Edad</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($consulta_libros->num_rows >0){
                            while($lb = $consulta_libros->fetch_assoc()){

                    ?>
                    <tr>
                        <td><?php echo $lb['titulo'] ?></td>
                        <td><?php echo $lb['autor'] ?></td>
                        <td><?php echo $lb['ano'] ?></td>
                        <td><?php echo $lb['idioma'] ?></td>
                    </tr>
                    
                    <?php }} ?>
                </tbody>
            </table>
        </div>
        <form action="eliminar_libro.php" method="POST">
      <div class=formulario>
        <div class="form-group">
          <label for="Titulo">Titulo</label>
          <input type="text" name="titulo" required="" placeholder="nombre del libro">
        </div>
          <button type="Eliminar Libro" class="btn btn-primary">Eliminar Libro</button>
        </div>
      </div>
    </div>
    <div class="botones">
        <a href="index.php"> Agregar libro </a>
    </div>

</body>
</html>